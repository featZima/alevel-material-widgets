package demo.application.materialwidgets;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class MyFragment extends Fragment {

    CustomWidget customWidget;

    private String getTitle() {
        return getArguments().getString(ARG_TITLE);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_layout, container, false);
        TextView titleTextView = fragmentView.findViewById(R.id.titleTextView);
        customWidget = fragmentView.findViewById(R.id.customWidget);
        customWidget.setFirstLine("!!!!!! AAAAA");
        customWidget.setCustomWidgetListener(new CustomWidgetListener() {
            @Override
            public void onClick() {
                Toast.makeText(getActivity(), "!!!!", Toast.LENGTH_LONG).show();
            }
        });
        fragmentView.findViewById(R.id.customView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        titleTextView.setText(getTitle());
        return fragmentView;
    }

    private static final String ARG_TITLE = "argTitle";

    public static MyFragment getInstance(String title) {
        Bundle arguments = new Bundle();
        arguments.putString(ARG_TITLE, title);
        MyFragment myFragment = new MyFragment();
        myFragment.setArguments(arguments);
        return myFragment;
    }
}
