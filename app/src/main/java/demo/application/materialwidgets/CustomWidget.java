package demo.application.materialwidgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class CustomWidget extends FrameLayout {

    protected TextView firstLineTextView;
    protected CustomWidgetListener customWidgetListener;

    public CustomWidget(@NonNull Context context) {
        super(context);
        init();
    }

    public CustomWidget(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomWidget(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setCustomWidgetListener(CustomWidgetListener customWidgetListener) {
        this.customWidgetListener = customWidgetListener;
    }

    public void init() {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        layoutInflater.inflate(R.layout.widget_custom, this, true);
        firstLineTextView = findViewById(R.id.firstLineTextView);
        firstLineTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                customWidgetListener.onClick();
            }
        });
    }

    public void setFirstLine(String value) {
        firstLineTextView.setText(value);
    }
}
